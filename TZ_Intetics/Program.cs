﻿using System;
using System.Collections.Generic;

namespace TZ_Intetics
{
    class Program
    {
        static void Main(string[] args)
        {
            Task task = new Task();

            task.Read();
        }
    }


    public class Task
    {

        public char[,] Matrix;

        public MatrixChar[,] matrixChar;

        public List<CharPosition> FinalyPositions = new List<CharPosition>();

        public void Read()
        {
            string matrixString;
            string word;

            Console.WriteLine("Введите строку матрицы:");

            matrixString = Console.ReadLine();

            if (Math.Sqrt(matrixString.Length) % 1 != 0 && matrixString.Length > 2)
            {
                Console.WriteLine("По условию задачи число символов должно быть N^2!");
                return;
            }
            Console.WriteLine("Введите слово:");

            word = Console.ReadLine();

            if (word.Length < 1) {
                Console.WriteLine("Поле не должно быть пустым!");
                return;
            }

            Initialize(matrixString, word);
        }

        public void Initialize(string _matrixString, string _word) {

            Matrix = GenerateMatrix(_matrixString);
            matrixChar = GenerateCharMatrix(Matrix, _word);

            Start(_word);

            Console.ReadKey();
        }

        public void Start(string _word)
        {
            CharPosition firsChar = FindFirstChar(matrixChar, _word[0]);

            if (firsChar != null)
            {
                JumpTo(firsChar, null, _word, 0);
            }
            else
            {
                Console.WriteLine("Слова не найдены!");
            }
        }

        public void JumpTo(CharPosition _curentChar, CharPosition _previousPosition, string _word, int _charIdexInWord)
        {

            if (_word.Length > _charIdexInWord + 1)
            {
                matrixChar[_curentChar.x, _curentChar.y].NeighboursList = FindNeighbours(matrixChar, _curentChar, _word[_charIdexInWord + 1]); // ищем сосдей
            }
            else
            {
                if (matrixChar[_curentChar.x, _curentChar.y].Char == _word[_charIdexInWord]) //проверяем последнюю букву
                {
                    FinalyPositions.Add(_curentChar);
                    Print(FinalyPositions);
                    return;
                }
                else
                {
                    matrixChar[_curentChar.x, _curentChar.y].isBadWay = true;
                    FinalyPositions.RemoveAt(_charIdexInWord);
                    JumpTo(matrixChar[_curentChar.x, _curentChar.y].PreviousPosition, null, _word, _charIdexInWord - 1);
                }
            }

            if (_previousPosition != null) // записываем предыдущую позицию если такая есть
            {
                matrixChar[_curentChar.x, _curentChar.y].PreviousPosition = _previousPosition;
            }

            if (matrixChar[_curentChar.x, _curentChar.y].NeighboursList.Count > 0)// если соседи есть
            {
                FinalyPositions.Add(_curentChar);
                CharPosition nextPosition = matrixChar[_curentChar.x, _curentChar.y].NeighboursList[0];
                matrixChar[_curentChar.x, _curentChar.y].PreviousPosition = _curentChar;
                JumpTo(nextPosition, _curentChar, _word, _charIdexInWord + 1);
            }
            else //если соседей нет
            {
                matrixChar[_curentChar.x, _curentChar.y].isBadWay = true;
                FinalyPositions.RemoveAt(FinalyPositions.Count - 1);

                if (_previousPosition != null)
                {
                    JumpTo(matrixChar[_curentChar.x, _curentChar.y].PreviousPosition, null, _word, _charIdexInWord - 1);
                }
                else
                {
                    Start(_word);
                }
            }
        }

        public void Print(List<CharPosition> _positions)
        {
        
            Console.WriteLine("Ответ: ");
            
            for(int i = 0; i < _positions.Count; i++)
            {
                Console.Write("[" + _positions[i].x + "," + _positions[i].y + "]");

                if(i < _positions.Count - 1)
                {
                    Console.Write("->");
                }
            }
        }

        // Data generate
        public char[,] GenerateMatrix(string _inputString)
        {
            int counter = 0;

            if (Math.Sqrt(_inputString.Length) % 1 == 0)
            {
                int countInLine = (int)Math.Sqrt(_inputString.Length);

                char[,] _matrix = new char[countInLine, countInLine];

                for (int x = 0; x < countInLine; x++)
                {
                    for (int y = 0; y < countInLine; y++)
                    {
                        _matrix[x, y] = _inputString[counter];
                        counter++;
                    }
                }
                return _matrix;
            }
            return null;
        }

        public MatrixChar[,] GenerateCharMatrix(char[,] _matrix, string _word)
        {

            MatrixChar[,] _matrixChar = new MatrixChar[_matrix.GetLength(0), _matrix.GetLength(1)];

            for (int i = 0; i < _word.Length; i++)
            {
                for (int x = 0; x < _matrix.GetLength(0); x++)
                {
                    for (int y = 0; y < _matrix.GetLength(1); y++)
                    {
                        if (_matrix[x, y] == _word[i])
                        {
                            _matrixChar[x, y] = new MatrixChar(_matrix[x, y], new CharPosition(x, y));
                        }
                    }
                }
            }

            return _matrixChar;
        }

        // Neighbour
        public List<CharPosition> FindNeighbours(MatrixChar[,] _matrix, CharPosition _curentPosition, char _targetChar) {

            List<CharPosition> neighboursPosition = new List<CharPosition>();

            CharPosition setPosition;

            setPosition = new CharPosition(_curentPosition.x + 1, _curentPosition.y);
            if (setPosition.x < _matrix.GetLength(0) && setPosition.y < _matrix.GetLength(1))
            {
                if (CheckNeigbourhoodChar(_matrix, setPosition.x, setPosition.y, _targetChar) != null)
                    neighboursPosition.Add(CheckNeigbourhoodChar(_matrix, setPosition.x, setPosition.y, _targetChar));
            }

            setPosition = new CharPosition(_curentPosition.x - 1, _curentPosition.y);
            if (setPosition.x >= 0 && setPosition.y < _matrix.GetLength(1))
            {
                if (CheckNeigbourhoodChar(_matrix, setPosition.x, setPosition.y, _targetChar) != null)
                    neighboursPosition.Add(CheckNeigbourhoodChar(_matrix, setPosition.x, setPosition.y, _targetChar));
            }

            setPosition = new CharPosition(_curentPosition.x, _curentPosition.y + 1);
            if (setPosition.x < _matrix.GetLength(0) && setPosition.y < _matrix.GetLength(1))
            {
                if (CheckNeigbourhoodChar(_matrix, setPosition.x, setPosition.y, _targetChar) != null)
                    neighboursPosition.Add(CheckNeigbourhoodChar(_matrix, setPosition.x, setPosition.y, _targetChar));
            }

            setPosition = new CharPosition(_curentPosition.x, _curentPosition.y - 1);
            if (setPosition.x < _matrix.GetLength(0) && 0 <= setPosition.y)
            {
                if (CheckNeigbourhoodChar(_matrix, setPosition.x, setPosition.y, _targetChar) != null)
                    neighboursPosition.Add(CheckNeigbourhoodChar(_matrix, setPosition.x, setPosition.y, _targetChar));
            }

            return neighboursPosition;
        }

        public CharPosition CheckNeigbourhoodChar(MatrixChar[,] _matrix, int _posX, int _posY, char _targetChar)
        {
            if (_matrix[_posX, _posY] != null && _matrix[_posX, _posY].Char == _targetChar)
            {
                if (_matrix[_posX, _posY].isBadWay == false)
                    return new CharPosition(_posX, _posY);
            }
            return null;
        }

        //Fide first
        public CharPosition FindFirstChar(MatrixChar[,] _matrix, char _targetChar)
        {
            for (int x = 0; x < _matrix.GetLength(0); x++)
            {
                for (int y = 0; y < _matrix.GetLength(1); y++)
                {
                    if (_matrix[x, y] != null)
                    {
                        if (_matrix[x, y].Char == _targetChar && _matrix[x, y].isBadWay == false)
                        {
                            return _matrix[x, y].position;
                        }
                    }
                }
            }
            return null; // если он сюда пиршёл, значит все данные неверные
        }

    }

    public class MatrixChar
    {
        public char Char;
        public CharPosition position;
        public bool isBadWay = false;

        public CharPosition PreviousPosition;

        public List<CharPosition> NeighboursList = new List<CharPosition>();

        public MatrixChar(char _char, CharPosition _postion)
        {
            Char = _char;
            position = _postion;
        }
    }

    public class CharPosition
    {
        public int x;
        public int y;

        public CharPosition(int _x, int _y)
        {
            x = _x;
            y = _y;
        }
    }
}
